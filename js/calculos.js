// Obtenemos los elementos del DOM
const valorAutoInput = document.getElementById("valorauto");
const porPagoinicialInput = document.getElementById("porpagoinicial");
const plazosSelect = document.getElementById("plazos");
const pagoInicialInput = document.getElementById("pagoinicial");
const totalAFinanciarInput = document.getElementById("totalafinanciar");
const pagoMensualInput = document.getElementById("pagomensual");
const btnCalcular = document.getElementById("btncalcular");

// Función para calcular la cotización
function calcularCotizacion() {
  // Obtenemos los valores de los inputs
  const valorAuto = parseFloat(valorAutoInput.value);
  const porPagoinicial = parseFloat(porPagoinicialInput.value) / 100;
  const plazos = parseFloat(plazosSelect.value);

  // Calculamos la cotización
  const pagoInicial = valorAuto * porPagoinicial;
  const totalAFinanciar = valorAuto - pagoInicial;
  const tasaInteresMensual = 0.02;
  const pagoMensual = totalAFinanciar * (tasaInteresMensual / (1 - Math.pow(1 + tasaInteresMensual, -plazos)));

  // Actualizamos los valores de los inputs
  pagoInicialInput.value = pagoInicial.toFixed(2);
  totalAFinanciarInput.value = totalAFinanciar.toFixed(2);
  pagoMensualInput.value = pagoMensual.toFixed(2);
}

// Asignamos el evento al botón "Calcular"
btnCalcular.addEventListener("click", calcularCotizacion);